package com.acemirr.learningpath4sub2.stackwidgets

import android.content.Intent
import android.widget.RemoteViewsService
import com.acemirr.learningpath4sub2.utils.Constant.WIDGET_LOCAL_EXTRA

class HomeWidgetService : RemoteViewsService() {
    override fun onGetViewFactory(intent: Intent?): RemoteViewsFactory {

        val listExtra: ArrayList<String> = intent?.getStringArrayListExtra(WIDGET_LOCAL_EXTRA) as ArrayList<String>
        return HomeRemoteFactory(this.applicationContext, listExtra)
    }
}