package com.acemirr.learningpath4sub2.stackwidgets

import android.app.job.JobInfo
import android.app.job.JobScheduler
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.os.Build
import android.os.Bundle
import android.widget.RemoteViews
import android.widget.RemoteViewsService
import com.acemirr.learningpath4sub2.R
import com.acemirr.learningpath4sub2.data.db.UserDatabase
import com.acemirr.learningpath4sub2.utils.Constant.WIDGET_USERNAME_EXTRA
import com.acemirr.learningpath4sub2.utils.logError
import com.acemirr.learningpath4sub2.utils.showToast
import com.squareup.picasso.Picasso
import com.squareup.picasso.Transformation
import jp.wasabeef.picasso.transformations.CropCircleTransformation

class HomeRemoteFactory(private val context: Context, private var imgUrlList: List<String>) :
    RemoteViewsService.RemoteViewsFactory {
    companion object {
        val homeWidgetItems = ArrayList<Bitmap>()
        val list = ArrayList<String>()

        private const val JOB_ID = 100
        private const val SCHEDULE_OF_PERIOD = 180000L
    }

    override fun onCreate() {
        logError("ON CREATE") // pakai logError karena warnanya merah mudah dikenali :D
        try {
            imgUrlList.let { list.addAll(it) }
            startJob(context)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun getLoadingView(): RemoteViews? {
        return null
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun onDataSetChanged() {
        homeWidgetItems.clear()
        val db = context.let { UserDatabase(it).getUserDao() }
        val listed = db.getAllUserDataWidget()
        val transformation: Transformation = CropCircleTransformation()
        try {
            listed.forEach {
                homeWidgetItems.add(
                    Picasso.get()
                        .load(it.avatarUrl)
                        .transform(transformation)
                        .get()
                )
            }
            logError("WIDGET ITEMS$homeWidgetItems")
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun hasStableIds(): Boolean {
        return false
    }

    override fun getViewAt(position: Int): RemoteViews {
            val rv = RemoteViews(context.packageName, R.layout.widget_item)
        if (count >0)
            rv.setImageViewBitmap(R.id.ivWidgetItem, homeWidgetItems[position])

            val extras = Bundle()
            extras.putInt(WIDGET_USERNAME_EXTRA, (position+1))
            val fillInIntent = Intent()
            fillInIntent.putExtras(extras)
            rv.setOnClickFillInIntent(R.id.ivWidgetItem, fillInIntent)
            return rv
    }

    override fun getCount(): Int {
        return homeWidgetItems.size
    }

    override fun getViewTypeCount(): Int {
        return 1
    }

    override fun onDestroy() {
        try {
            cancelJob(context)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun startJob(context: Context) {
        val mServiceComponent = ComponentName(context, UpdateHomeWidget::class.java)
        val builder = JobInfo.Builder(JOB_ID, mServiceComponent)
        builder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_NONE)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            builder.setPeriodic(900000) //15 minutes
        } else {
            builder.setPeriodic(SCHEDULE_OF_PERIOD) //3 minutes
        }
        val jobScheduler = context.getSystemService(Context.JOB_SCHEDULER_SERVICE) as JobScheduler
        jobScheduler.schedule(builder.build())
        context.showToast(context.getString(R.string.widget_service_started))
    }

    private fun cancelJob(context: Context) {
        val tm = context.getSystemService(Context.JOB_SCHEDULER_SERVICE) as JobScheduler
        tm.cancel(JOB_ID)
        context.showToast(context.getString(R.string.widget_service_canceled))
    }
}