package com.acemirr.learningpath4sub2.stackwidgets

import android.app.job.JobParameters
import android.app.job.JobService
import android.appwidget.AppWidgetManager
import android.content.ComponentName
import android.widget.RemoteViews
import com.acemirr.learningpath4sub2.R

class UpdateHomeWidget : JobService() {
    override fun onStartJob(params: JobParameters?): Boolean {
        val manager = AppWidgetManager.getInstance(this)
        val widget = ComponentName(this, HomeWidget::class.java)
        val ids = manager.getAppWidgetIds(widget)

        val view = RemoteViews(packageName, R.layout.home_widget)
        manager.notifyAppWidgetViewDataChanged(ids, R.id.stackWidgetFavorites)
        manager.updateAppWidget(widget, view)
        jobFinished(params, false)
        return true
    }

    override fun onStopJob(params: JobParameters?): Boolean {
        return true
    }
}