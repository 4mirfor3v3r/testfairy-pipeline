package com.acemirr.learningpath4sub2.stackwidgets

import android.app.PendingIntent
import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.Context
import android.content.Intent
import android.widget.RemoteViews
import androidx.core.net.toUri
import com.acemirr.learningpath4sub2.R
import com.acemirr.learningpath4sub2.data.db.UserDatabase
import com.acemirr.learningpath4sub2.utils.Constant.WIDGET_CLICK
import com.acemirr.learningpath4sub2.utils.Constant.WIDGET_ID_EXTRA
import com.acemirr.learningpath4sub2.utils.Constant.WIDGET_LOCAL_EXTRA
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch


class HomeWidget : AppWidgetProvider() {

    override fun onUpdate(context: Context, appWidgetManager: AppWidgetManager, appWidgetIds: IntArray) {
        for (appWidgetId in appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId)
        }
    }

    override fun onEnabled(context: Context) {
        // Enter relevant functionality for when the first widget is created
    }

    override fun onDisabled(context: Context) {
        // Enter relevant functionality for when the last widget is disabled
    }

    override fun onReceive(context: Context?, intent: Intent?) {
        super.onReceive(context, intent)
        if (intent != null) {
            if (intent.action==WIDGET_CLICK){
                val appWidgetManager = AppWidgetManager.getInstance(context)
                val views = RemoteViews(context?.packageName, R.layout.home_widget)
                val appWidgetId = intent.getIntExtra(WIDGET_ID_EXTRA, 0)
                try {
                    val id: Int = intent.getIntExtra(WIDGET_ID_EXTRA,0)
                    appWidgetManager.notifyAppWidgetViewDataChanged(id,R.id.stackWidgetFavorites)
                }catch (e:Exception){
                    e.printStackTrace()
                }
                appWidgetManager.updateAppWidget(appWidgetId, views)
            }
        }
    }
    private fun getPendingSelfIntent(context: Context, appWidgetId: Int, action: String, widgetLocalExtra: ArrayList<String>): PendingIntent {
        val intent = Intent(context, javaClass)
        intent.action = action
        intent.putExtra(WIDGET_ID_EXTRA, appWidgetId)
        intent.putExtra(WIDGET_LOCAL_EXTRA,widgetLocalExtra)
        return PendingIntent.getBroadcast(context, appWidgetId, intent, 0)
    }
    private fun updateAppWidget(context: Context, appWidgetManager: AppWidgetManager, appWidgetId: Int) {
        try {
            val views = RemoteViews(context.packageName, R.layout.home_widget)
            CoroutineScope(Dispatchers.IO).launch {
                val imgList = ArrayList<String>()
                val db = UserDatabase(context).getUserDao()
                val listUser = db.getAllUserData()

                listUser.forEach {
                    it.avatarUrl?.let { it1 -> imgList.add(it1) }
                }
                delay(800)
                val intent = Intent(context, HomeWidgetService::class.java)
                intent.putExtra(WIDGET_ID_EXTRA, appWidgetId)
                intent.putStringArrayListExtra(WIDGET_LOCAL_EXTRA, imgList)
                intent.data = intent.toUri(Intent.URI_INTENT_SCHEME).toUri()

                views.setRemoteAdapter(R.id.stackWidgetFavorites, intent)
                views.setEmptyView(R.id.stackWidgetFavorites, R.id.tvWidgetFavoritesEmpty)

                views.setOnClickPendingIntent(R.id.btnWidgetFavoritesRefresh, getPendingSelfIntent(context, appWidgetId, WIDGET_CLICK, imgList))

                appWidgetManager.updateAppWidget(appWidgetId, views)
            }
        }catch (e:Exception){
            e.printStackTrace()
        }
    }



}
