package com.acemirr.learningpath4sub2

import android.content.Context
import androidx.multidex.MultiDex
import androidx.multidex.MultiDexApplication
import com.acemirr.learningpath4sub2.data.api.ApiService
import com.acemirr.learningpath4sub2.data.api.NetworkConfig
import com.acemirr.learningpath4sub2.data.db.UserDatabase
import com.acemirr.learningpath4sub2.data.repository.DetailRepo
import com.acemirr.learningpath4sub2.data.repository.FavoriteRepo
import com.acemirr.learningpath4sub2.data.repository.ListRepo
import com.acemirr.learningpath4sub2.data.repository.SearchRepo
import com.acemirr.learningpath4sub2.ui.base.ViewModelFactory
import com.acemirr.learningpath4sub2.utils.PreferencesHelper
import com.testfairy.TestFairy
import org.kodein.di.*
import org.kodein.di.android.x.androidXModule

class App : MultiDexApplication(), DIAware {
    override val di: DI = DI.lazy {

        import(androidXModule(this@App))

        bind<ApiService>() with singleton { NetworkConfig.api() }

        bind() from singleton { UserDatabase(instance()) }
        bind() from singleton { instance<UserDatabase>().getUserDao() }

        bind() from provider { ViewModelFactory(instance(),instance(),instance()) }

        bind() from singleton { ListRepo(instance(),instance(),instance()) }
        bind() from singleton { DetailRepo(instance(),instance(),instance()) }
        bind() from singleton { FavoriteRepo(instance(),instance()) }
        bind() from singleton { SearchRepo(instance(),instance(),instance()) }

    }
    companion object {
        var prefHelper: PreferencesHelper? = null
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        TestFairy.begin(this, "SDK-kYtGo3pS")
        MultiDex.install(this)
    }

    override fun onCreate() {
        super.onCreate()
        prefHelper = PreferencesHelper(this)
    }
}