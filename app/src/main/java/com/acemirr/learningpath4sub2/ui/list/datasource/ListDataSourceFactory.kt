package com.acemirr.learningpath4sub2.ui.list.datasource

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.acemirr.learningpath4sub2.data.model.User
import com.acemirr.learningpath4sub2.data.repository.ListRepo

class ListDataSourceFactory(private val pagingRepo: ListRepo) : DataSource.Factory<Int, User>() {

    val listDataSourceLiveData: MutableLiveData<ListDataSource> = MutableLiveData()
    override fun create(): DataSource<Int, User> {
        val pagingDataSource = ListDataSource(pagingRepo)
        listDataSourceLiveData.postValue(pagingDataSource)
        return pagingDataSource
    }

}