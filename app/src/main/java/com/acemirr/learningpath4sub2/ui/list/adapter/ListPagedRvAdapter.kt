package com.acemirr.learningpath4sub2.ui.list.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.databinding.DataBindingUtil
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.acemirr.learningpath4sub2.R
import com.acemirr.learningpath4sub2.data.model.User
import com.acemirr.learningpath4sub2.databinding.ItemListPagingBinding
import com.acemirr.learningpath4sub2.listener.PagingOnItemClick
import com.acemirr.learningpath4sub2.utils.AdapterCallback
import com.acemirr.learningpath4sub2.utils.LoadingState

class ListPagedRvAdapter :
    PagedListAdapter<User, RecyclerView.ViewHolder>(AdapterCallback.DiffUserCallback) {
    companion object {
        const val VIEW_TYPE_ITEM = 1
        const val VIEW_TYPE_LOAD = 2
    }

    private var loadingState = LoadingState.LOADING
    private var lastPosition = -1

    private var onItemClicked: PagingOnItemClick? = null
    fun setOnItemClick(onItemClick: PagingOnItemClick) {
        this.onItemClicked = onItemClick
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return if (viewType == VIEW_TYPE_ITEM) {
            val binding: ItemListPagingBinding =
                DataBindingUtil.inflate(inflater, R.layout.item_list_paging, parent, false)
            PagingHolder(binding)
        } else {
            val x = inflater.inflate(R.layout.item_load_more, parent, false)
            LoadMoreHolder(x)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is PagingHolder) {
            try {
                val pagingItem: User? = getItem(holder.adapterPosition)
                pagingItem?.let {
                    holder.bindItem(
                        pagingItem, onItemClicked
                    )
                    setAnimation(
                        holder.itemView.context.applicationContext,
                        holder.itemView,
                        holder.adapterPosition
                    )
                }
            } catch (e: IndexOutOfBoundsException) {
                e.printStackTrace()
            }
        }
    }

    override fun onViewDetachedFromWindow(holder: RecyclerView.ViewHolder) {
        holder.itemView.clearAnimation()
        super.onViewDetachedFromWindow(holder)
    }

    override fun getItemViewType(position: Int): Int {
        return if (position < super.getItemCount()) VIEW_TYPE_ITEM else VIEW_TYPE_LOAD
    }

    override fun getItemCount(): Int {
        return super.getItemCount() + if (hasFooter()) 1 else 0
    }

    private fun hasFooter(): Boolean {
        return super.getItemCount() != 0 && loadingState == LoadingState.LOADING
    }

    private fun setAnimation(context: Context, viewToAnimate: View, position: Int) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            val animation: Animation =
                AnimationUtils.loadAnimation(context, R.anim.rv_paging_list_animation)
            viewToAnimate.startAnimation(animation)
            lastPosition = position
        }
    }

    class PagingHolder(val binding: ItemListPagingBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bindItem(pagingModel: User, onItemClicked: PagingOnItemClick?) {
            binding.data = pagingModel
            binding.action = onItemClicked

            binding.executePendingBindings()
        }
    }

    class LoadMoreHolder(x: View) : RecyclerView.ViewHolder(x)

}