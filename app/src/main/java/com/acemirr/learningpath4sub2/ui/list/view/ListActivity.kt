package com.acemirr.learningpath4sub2.ui.list.view

import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.provider.Settings
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.acemirr.learningpath4sub2.R
import com.acemirr.learningpath4sub2.data.api.ApiHelper
import com.acemirr.learningpath4sub2.data.api.ApiServiceImpl
import com.acemirr.learningpath4sub2.data.model.User
import com.acemirr.learningpath4sub2.databinding.ActivityListBinding
import com.acemirr.learningpath4sub2.listener.PagingOnItemClick
import com.acemirr.learningpath4sub2.ui.base.ViewModelFactory
import com.acemirr.learningpath4sub2.ui.detail.view.DetailActivity
import com.acemirr.learningpath4sub2.ui.favorite.view.FavoriteActivity
import com.acemirr.learningpath4sub2.ui.list.adapter.ListPagedRvAdapter
import com.acemirr.learningpath4sub2.ui.list.viewmodel.ListViewModel
import com.acemirr.learningpath4sub2.ui.searchview.view.SearchActivity
import com.acemirr.learningpath4sub2.utils.Constant.EXTRA_DATA_PAGING
import com.acemirr.learningpath4sub2.utils.appwidget.TimePickerFragment
import kotlinx.android.synthetic.main.activity_list.*


class ListActivity : AppCompatActivity() {
    private lateinit var viewModel: ListViewModel
    private lateinit var binding: ActivityListBinding
    private var adapter = ListPagedRvAdapter()

    var dialogTimeListener: TimePickerFragment.DialogTimeListener? = null
    fun setDialogListener(listener: TimePickerFragment.DialogTimeListener) {
        this.dialogTimeListener = listener
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_list)
        viewModel = ViewModelProvider(
            this,
            ViewModelFactory(this, ApiHelper(ApiServiceImpl()), lifecycleScope)
        )
            .get(ListViewModel::class.java)
        binding.vm = viewModel
        setSupportActionBar(toolbar)

        setupRecyclerView()
    }

    override fun onStart() {
        super.onStart()

        observeLiveData()
        handleButtonIntent()
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        recreate()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        this.menuInflater.inflate(R.menu.menu_list, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menuListSearch -> {
                startActivity(Intent(this, SearchActivity::class.java))
                overridePendingTransition(R.anim.slide_from_top, R.anim.slide_to_bottom)
            }
            R.id.menuListChangeLanguage -> {
                startActivity(Intent(Settings.ACTION_LOCALE_SETTINGS))
            }
            R.id.menuListReminder -> {
                setLayoutHidden(true)
                supportFragmentManager.beginTransaction()
                    .replace(binding.flList.id, DialogReminder())
                    .commit()
            }
        }
        return true
    }

    fun setLayoutHidden(isHidden: Boolean) {
        if (!isHidden) {
            supportFragmentManager.findFragmentById(binding.flList.id)?.let {
                supportFragmentManager.beginTransaction()
                    .remove(it)
                    .commit()
            }
        }

        viewModel.isSuppressed.set(isHidden)
    }

    override fun onBackPressed() {
        val isSuppressed = viewModel.isSuppressed.get()
        if (isSuppressed != null) {
            if (isSuppressed) {
                setLayoutHidden(false)
            }
            else{
                super.onBackPressed()
            }
        }else{
            super.onBackPressed()
        }
    }

    private fun setupRecyclerView() {
        val layoutManager = LinearLayoutManager(this)
        binding.rvListPaging.layoutManager = layoutManager

        adapter.setOnItemClick(object : PagingOnItemClick {
            override fun onItemClicked(view: View, user: User) {
                onItemClicked(user)
            }
        })
        binding.rvListPaging.adapter = adapter
    }

    private fun observeLiveData() {
        viewModel.getLoadingState(this)
        viewModel.pagingList.observe(this, Observer { network ->
            adapter.submitList(network)
        })
    }

    private fun onItemClicked(pagingModel: User) {
        try {
            if (!viewModel.isSuppressed.get()!!) {
                val intent = Intent(this, DetailActivity::class.java)
                intent.putExtra(EXTRA_DATA_PAGING, pagingModel)
                startActivity(intent)
                overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left)
            }
        }catch (e:Exception){
            e.printStackTrace()
        }
    }

    private fun handleButtonIntent() {
//        normally create single live event class to pass data over viewmodel
        binding.fabListPaging.setOnClickListener {
            startActivity(Intent(this, FavoriteActivity::class.java))
            overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right)
        }
    }




}
