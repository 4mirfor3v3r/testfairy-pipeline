package com.acemirr.learningpath4sub2.ui.detail.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.Observable
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.acemirr.learningpath4sub2.R
import com.acemirr.learningpath4sub2.data.api.ApiHelper
import com.acemirr.learningpath4sub2.data.api.ApiServiceImpl
import com.acemirr.learningpath4sub2.data.db.entity.LocalModel
import com.acemirr.learningpath4sub2.data.model.User
import com.acemirr.learningpath4sub2.databinding.ActivityDetailBinding
import com.acemirr.learningpath4sub2.listener.FollowerCallback
import com.acemirr.learningpath4sub2.listener.FollowingCallback
import com.acemirr.learningpath4sub2.ui.base.ViewModelFactory
import com.acemirr.learningpath4sub2.ui.detail.adapter.DetailTabAdapter
import com.acemirr.learningpath4sub2.ui.detail.viewmodel.DetailViewModel
import com.acemirr.learningpath4sub2.utils.Constant
import com.acemirr.learningpath4sub2.utils.logDebug

class DetailActivity : AppCompatActivity() {
    private lateinit var viewModel: DetailViewModel
    private lateinit var binding: ActivityDetailBinding
    var extraUser: User? = null

    var extraLocalModel: LocalModel? = null

    private var followingCallback: FollowingCallback? = null
    private var followerCallback: FollowerCallback? = null

    fun onFollowingCallback(listener: FollowingCallback) {
        this.followingCallback = listener
    }

    fun onFollowerCallback(listener: FollowerCallback) {
        this.followerCallback = listener
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProvider(
            this,
            ViewModelFactory(this, ApiHelper(ApiServiceImpl()), lifecycleScope)
        )
            .get(DetailViewModel::class.java)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_detail)
        binding.vm = viewModel

        val adapter = DetailTabAdapter(this, supportFragmentManager)
        binding.vpDetail.adapter = adapter
        binding.tabDetail.setupWithViewPager(binding.vpDetail)

        try {
            extraUser = intent.getParcelableExtra(Constant.EXTRA_DATA_PAGING)
            this.extraUser!!.login.let { viewModel.getUserDetail(it) }
            logDebug("EXTRA USER")
        } catch (e: NullPointerException) {
            e.printStackTrace()
            extraLocalModel = intent.getParcelableExtra(Constant.EXTRA_LOCAL_DATA)
            this.extraLocalModel!!.username?.let { viewModel.getLocalUserDetail(it) }
            logDebug("EXTRA LOCAL USER")
        }
    }

    override fun onStart() {
        super.onStart()
        observeData()
    }

    private fun observeData() {
        if (extraUser != null) {
            viewModel.isLiked.addOnPropertyChangedCallback(object :
                Observable.OnPropertyChangedCallback() {
                override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                    val isLike = viewModel.isLiked.get()
                    logDebug("OBSERVAVLE CHANGED $isLike")
                    if (isLike != null) {
                        if (isLike) {
                            viewModel.users?.let { followingCallback?.onLikesAdded(it) }
                            viewModel.users?.let { followerCallback?.onLikesAdded(it) }
                        }
                    }
                }
            })
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right)
    }
}
