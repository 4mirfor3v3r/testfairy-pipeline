package com.acemirr.learningpath4sub2.ui.favorite.view

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.PopupMenu
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.acemirr.learningpath4sub2.R
import com.acemirr.learningpath4sub2.data.api.ApiHelper
import com.acemirr.learningpath4sub2.data.api.ApiServiceImpl
import com.acemirr.learningpath4sub2.data.db.entity.LocalModel
import com.acemirr.learningpath4sub2.databinding.ActivityFavoriteBinding
import com.acemirr.learningpath4sub2.listener.FavoriteOnItemClick
import com.acemirr.learningpath4sub2.ui.base.ViewModelFactory
import com.acemirr.learningpath4sub2.ui.detail.view.DetailActivity
import com.acemirr.learningpath4sub2.ui.favorite.adapter.FavoriteRvAdapter
import com.acemirr.learningpath4sub2.ui.favorite.viewmodel.FavoriteActivityViewModel
import com.acemirr.learningpath4sub2.utils.Constant

class FavoriteActivity : AppCompatActivity() {
    private lateinit var viewModel: FavoriteActivityViewModel
    private lateinit var binding: ActivityFavoriteBinding

    private var adapter = FavoriteRvAdapter()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_favorite)
        viewModel = ViewModelProvider(
            this,
            ViewModelFactory(this, ApiHelper(ApiServiceImpl()), lifecycleScope)
        )
            .get(FavoriteActivityViewModel::class.java)
        binding.vm = viewModel

        setupRecyclerView()
    }

    override fun onStart() {
        super.onStart()
        observeData()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left)
    }

    private fun observeData() {
        viewModel.getLoadingState(this)
        viewModel.getLocalData()
        viewModel.listLocal.observe(this, Observer {
            adapter.submitList(it)
        })
    }

    private fun setupRecyclerView() {
        val layoutManager = LinearLayoutManager(this)
        binding.rvFavorite.layoutManager = layoutManager
        adapter.setOnItemClick(object : FavoriteOnItemClick {
            override fun onItemClicked(view: View, user: LocalModel) {
                onItemClick(user)
            }

            override fun onLongClicked(view: View, user: LocalModel): Boolean {
                onLongClick(view, user)
                return true
            }

            override fun onTrashClicked(view: View, user: LocalModel) {
                onTrashClick(view, user)
            }
        })
        binding.rvFavorite.adapter = adapter
    }

    private fun onTrashClick(view: View, user: LocalModel) {
        view.isEnabled = false
        viewModel.deleteFromDatabase(user)
    }

    private fun onLongClick(view: View, user: LocalModel) {
        val menu = PopupMenu(this, view)
        menuInflater.inflate(R.menu.menu_fav, menu.menu)
        menu.show()

        menu.setOnMenuItemClickListener {
            view.isEnabled = false
            viewModel.deleteFromDatabase(user)
            true
        }
    }

    private fun onItemClick(model: LocalModel) {
        val intent = Intent(this, DetailActivity::class.java)
        intent.putExtra(Constant.EXTRA_LOCAL_DATA, model)
        startActivity(intent)
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left)
    }
}
