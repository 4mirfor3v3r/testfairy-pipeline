package com.acemirr.learningpath4sub2.ui.detail.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.acemirr.learningpath4sub2.R
import com.acemirr.learningpath4sub2.data.db.entity.LocalUser
import com.acemirr.learningpath4sub2.databinding.ItemDetailUserBinding
import com.acemirr.learningpath4sub2.utils.AdapterCallback

class DetailFragmentFollowingRvAdapter :
    ListAdapter<LocalUser, DetailFragmentFollowingRvAdapter.Holder>(AdapterCallback.DiffLocalUserCallback) {
    class Holder(private val itemGridBinding: ItemDetailUserBinding): RecyclerView.ViewHolder(itemGridBinding.root) {
        fun bindView(placeList: LocalUser) {
            itemGridBinding.data = placeList
            itemGridBinding.executePendingBindings()
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val binding = DataBindingUtil.inflate<ItemDetailUserBinding>(LayoutInflater.from(parent.context), R.layout.item_detail_user,parent,false)
        return Holder(binding)
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        val gridModel = getItem(holder.adapterPosition)
        holder.bindView(gridModel)
    }
}