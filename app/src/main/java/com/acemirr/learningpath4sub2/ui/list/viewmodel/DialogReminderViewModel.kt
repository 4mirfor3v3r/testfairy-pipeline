package com.acemirr.learningpath4sub2.ui.list.viewmodel

import android.content.Context
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.acemirr.learningpath4sub2.App
import com.acemirr.learningpath4sub2.R
import com.acemirr.learningpath4sub2.services.receiver.AlarmReceiver
import com.acemirr.learningpath4sub2.utils.Constant
import com.acemirr.learningpath4sub2.utils.Constant.PREF_TIME_KEY
import com.acemirr.learningpath4sub2.utils.Constant.TIME_PICKER_REPEAT_TAG
import com.acemirr.learningpath4sub2.utils.Constant.TYPE_REPEATING
import com.acemirr.learningpath4sub2.utils.logError
import java.text.SimpleDateFormat
import java.util.*

//Log Error karena warnanya merah :D
class DialogReminderViewModel : ViewModel() {
    var isChecked = ObservableField(false)
    val time = MutableLiveData<String>()
    private val alarmReceiver = AlarmReceiver()
    private val dateFormat = SimpleDateFormat("HH:mm", Locale.getDefault())

    fun getPrefValue() {
        val prefTime = App.prefHelper?.getString(PREF_TIME_KEY)
        if (prefTime != null && prefTime != "") {
            time.postValue(prefTime)
        } else {
            val currentTime = Calendar.getInstance().time
            time.postValue(dateFormat.format(currentTime))
            App.prefHelper?.setString(PREF_TIME_KEY, dateFormat.format(currentTime))
        }
        val prefChecked = App.prefHelper?.getString(Constant.PREF_STATE_KEY)
        if (prefChecked != null && prefChecked != "") {
            isChecked.set(prefChecked.toBoolean())
        }
    }

    fun onSwitchChange(checked: Boolean) {
        isChecked.set(checked)
        App.prefHelper?.setString(Constant.PREF_STATE_KEY, checked.toString())
    }

    fun onBtnSaveClicked(context: Context) {
        val check = isChecked.get()
        if (check != null) {
            if (check) {
                time.value?.let {
                    alarmReceiver.setRepeatingAlarm(
                        context,
                        it, context.getString(R.string.reminder_message)
                    )
                    logError("ALARM SET UPED @$it")
                }
            } else {
                context.let { it1 -> alarmReceiver.cancelAlarm(it1, TYPE_REPEATING) }
            }
        }
    }

    fun onTimeSet(tag: String?, hourOfDay: Int, minute: Int) {
        val calendar = Calendar.getInstance()
        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay)
        calendar.set(Calendar.MINUTE, minute)

        when (tag) {
            TIME_PICKER_REPEAT_TAG -> {
                time.postValue(dateFormat.format(calendar.time))
                App.prefHelper?.setString(PREF_TIME_KEY, dateFormat.format(calendar.time))
            }
        }
    }

}