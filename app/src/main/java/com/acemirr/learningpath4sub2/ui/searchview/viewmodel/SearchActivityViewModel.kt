package com.acemirr.learningpath4sub2.ui.searchview.viewmodel

import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.acemirr.learningpath4sub2.data.model.SearchModel
import com.acemirr.learningpath4sub2.data.repository.SearchRepo

class SearchActivityViewModel(private val searchRepo: SearchRepo) : ViewModel() {
    val searchModel: MutableLiveData<SearchModel> = MutableLiveData()
    var isLoading = ObservableField<Boolean>()

    fun getAllUsers(query: String) {
        isLoading.set(true)
        searchRepo.searchUser(query, {
            searchModel.postValue(it)
        }, {
            isLoading.set(false)
        })
    }
}