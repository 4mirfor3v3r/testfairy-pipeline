package com.acemirr.learningpath4sub2.ui.detail.adapter

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.acemirr.learningpath4sub2.R
import com.acemirr.learningpath4sub2.ui.detail.view.FragmentDetailFollower
import com.acemirr.learningpath4sub2.ui.detail.view.FragmentDetailFollowing

class DetailTabAdapter(context: Context, fm: FragmentManager) : FragmentPagerAdapter(
    fm,
    BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
    private val tabTitles = arrayOf(
        context.getString(R.string.detail_tab_follower),
        context.getString(R.string.detail_tab_following)
    )

    override fun getItem(position: Int): Fragment {
        return if (!FragmentDetailFollowing().isAdded || !FragmentDetailFollower().isAdded) {
            if (position == 0) {
                FragmentDetailFollower()
            } else {
                FragmentDetailFollowing()
            }
        }else{
            Fragment()
        }
    }

    override fun getCount(): Int {
        return tabTitles.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return tabTitles[position]
    }
}