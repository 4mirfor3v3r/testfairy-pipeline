package com.acemirr.learningpath4sub2.ui.searchview.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.acemirr.learningpath4sub2.R
import com.acemirr.learningpath4sub2.data.model.User
import com.acemirr.learningpath4sub2.databinding.ItemListPagingBinding
import com.acemirr.learningpath4sub2.listener.PagingOnItemClick
import com.acemirr.learningpath4sub2.utils.AdapterCallback

class SearchRvAdapter :
    ListAdapter<User, SearchRvAdapter.Holder>(AdapterCallback.DiffUserCallback) {
    private var lastPosition = -1

    private var onItemClicked: PagingOnItemClick? = null
    fun setOnItemClick(onItemClick: PagingOnItemClick) {
        this.onItemClicked = onItemClick
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val binding = DataBindingUtil.inflate<ItemListPagingBinding>(
            LayoutInflater.from(parent.context), R.layout.item_list_paging, parent, false
        )
        return Holder(binding)
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        try {
            val user = getItem(holder.adapterPosition)
            holder.bindItem(user, onItemClicked)
            setAnimation(
                holder.itemView.context.applicationContext,
                holder.itemView,
                holder.adapterPosition
            )
        } catch (e: IndexOutOfBoundsException) {
            e.printStackTrace()
        }
    }

    private fun setAnimation(context: Context, viewToAnimate: View, position: Int) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            val animation: Animation =
                AnimationUtils.loadAnimation(context, R.anim.rv_paging_list_animation)
            viewToAnimate.startAnimation(animation)
            lastPosition = position
        }
    }

    class Holder(private val binding: ItemListPagingBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindItem(user: User, onItemClicked: PagingOnItemClick?) {
            binding.data = user
            binding.action = onItemClicked

            binding.executePendingBindings()
        }

    }
}