package com.acemirr.learningpath4sub2.ui.detail.view

import android.content.Context
import android.content.res.Configuration
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.acemirr.learningpath4sub2.R
import com.acemirr.learningpath4sub2.data.api.ApiHelper
import com.acemirr.learningpath4sub2.data.api.ApiServiceImpl
import com.acemirr.learningpath4sub2.data.db.entity.LocalModel
import com.acemirr.learningpath4sub2.databinding.FragmentDetailFollowerBinding
import com.acemirr.learningpath4sub2.listener.FollowerCallback
import com.acemirr.learningpath4sub2.ui.base.ViewModelFactory
import com.acemirr.learningpath4sub2.ui.detail.adapter.DetailFragmentFollowersRvAdapter
import com.acemirr.learningpath4sub2.ui.detail.viewmodel.FragmentDetailFollowerViewModel
import com.acemirr.learningpath4sub2.utils.logDebug

class FragmentDetailFollower : Fragment(), FollowerCallback {
    private lateinit var viewModel: FragmentDetailFollowerViewModel
    lateinit var binding: FragmentDetailFollowerBinding
    private lateinit var adapter: DetailFragmentFollowersRvAdapter
    private lateinit var ctx: Context
    private lateinit var detailActivity: DetailActivity

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_detail_follower, container, false)
        binding.lifecycleOwner = this

        ctx = (context as DetailActivity)
        detailActivity = (activity as DetailActivity)

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(
            this,
            ViewModelFactory(ctx, ApiHelper(ApiServiceImpl()), lifecycleScope)
        )
            .get(FragmentDetailFollowerViewModel::class.java)
        binding.vm = viewModel

        detailActivity.onFollowerCallback(this)

        setupRecyclerView()
        observeData()

        detailActivity.extraUser?.login?.let { viewModel.getFollowers(it) }
        detailActivity.extraLocalModel?.followersUrl?.let {
            viewModel.listFollowers.postValue(it)
            logDebug("EXECUTED ADD ALL ${viewModel.listFollowers.value}")
        }

    }

    override fun onResume() {
        super.onResume()
        binding.rvDetailFragFollower.startLayoutAnimation()
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        activity?.recreate()
    }

    private fun observeData() {
        viewModel.listFollowers.observe(viewLifecycleOwner, Observer {
            adapter.submitList(it)
            binding.rvDetailFragFollower.startLayoutAnimation()
        })
    }

    private fun setupRecyclerView() {
        val layoutManager = LinearLayoutManager(context)
        val divider = DividerItemDecoration(context,LinearLayoutManager.VERTICAL)
        binding.rvDetailFragFollower.layoutManager = layoutManager
        binding.rvDetailFragFollower.addItemDecoration(divider)
        adapter = DetailFragmentFollowersRvAdapter()

        binding.rvDetailFragFollower.adapter = adapter
    }

    override fun onLikesAdded(localModel: LocalModel) {
        try {
            viewModel.insertListToDb(viewLifecycleOwner, localModel)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

}
