package com.acemirr.learningpath4sub2.ui.favorite.viewmodel

import androidx.databinding.ObservableField
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import com.acemirr.learningpath4sub2.data.db.entity.LocalModel
import com.acemirr.learningpath4sub2.data.repository.FavoriteRepo
import com.acemirr.learningpath4sub2.utils.LoadingState

class FavoriteActivityViewModel(private val favoriteRepo: FavoriteRepo) : ViewModel() {
    var isLoading = ObservableField<Boolean>()
    var listLocal: MutableLiveData<List<LocalModel>> = MutableLiveData()

    fun getLocalData() {
        favoriteRepo.getLocalData {
            listLocal.postValue(it)
        }
    }

    fun getLoadingState(lifecycleOwner: LifecycleOwner) {
        favoriteRepo.loadingState.observe(lifecycleOwner, Observer {
            if (it == LoadingState.DONE)
                isLoading.set(false)
            else if (it == LoadingState.LOADING) {
                isLoading.set(true)
            }
        })
    }

    fun deleteFromDatabase(user: LocalModel) {
        favoriteRepo.deleteFavorites(user) {
            getLocalData()
        }
    }
}