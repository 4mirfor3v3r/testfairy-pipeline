package com.acemirr.learningpath4sub2.ui.favorite.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.acemirr.learningpath4sub2.R
import com.acemirr.learningpath4sub2.data.db.entity.LocalModel
import com.acemirr.learningpath4sub2.databinding.ItemFavoriteBinding
import com.acemirr.learningpath4sub2.listener.FavoriteOnItemClick
import com.acemirr.learningpath4sub2.utils.AdapterCallback


class FavoriteRvAdapter :
    ListAdapter<LocalModel, FavoriteRvAdapter.Holder>(AdapterCallback.DiffLocalmodelCallback) {
    private var lastPosition = -1

    private var onItemClicked: FavoriteOnItemClick? = null
    fun setOnItemClick(onItemClick: FavoriteOnItemClick) {
        this.onItemClicked = onItemClick
    }

    class Holder(private val binding: ItemFavoriteBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bindItem(user: LocalModel, onItemClick: FavoriteOnItemClick?) {
            binding.data = user
            binding.action = onItemClick

            binding.executePendingBindings()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val binding = DataBindingUtil.inflate<ItemFavoriteBinding>(
            LayoutInflater.from(parent.context),
            R.layout.item_favorite,
            parent,
            false
        )
        return Holder(binding)
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        try {
            val user = getItem(holder.adapterPosition)
            holder.bindItem(user, onItemClicked)

            setAnimation(
                holder.itemView.context.applicationContext,
                holder.itemView,
                holder.adapterPosition
            )
        } catch (e: IndexOutOfBoundsException) {
            e.printStackTrace()
        }
    }

    private fun setAnimation(context: Context, viewToAnimate: View, position: Int) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            val animation: Animation =
                AnimationUtils.loadAnimation(context, R.anim.rv_paging_list_animation)
            viewToAnimate.startAnimation(animation)
            lastPosition = position
        }
    }
}