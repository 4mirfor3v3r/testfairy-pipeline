package com.acemirr.learningpath4sub2.ui.detail.viewmodel

import androidx.databinding.ObservableField
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import com.acemirr.learningpath4sub2.data.db.entity.LocalModel
import com.acemirr.learningpath4sub2.data.db.entity.LocalUser
import com.acemirr.learningpath4sub2.data.model.User
import com.acemirr.learningpath4sub2.data.model.UserListModel
import com.acemirr.learningpath4sub2.data.repository.DetailRepo
import com.acemirr.learningpath4sub2.utils.logDebug

class FragmentDetailFollowerViewModel(private val detailRepo: DetailRepo) : ViewModel() {
    val listFollowers = MutableLiveData<List<LocalUser>>()
    private val listFol = MutableLiveData<List<User>>()
    var isLoading = ObservableField<Boolean>()

    fun getFollowers(username: String) {
        isLoading.set(true)
        detailRepo.getFollowers(username, {
            convertUserToLocalUser(it)
            listFol.postValue(it)
        }, {
            isLoading.set(false)
        })
    }

    private fun convertUserToLocalUser(userListModel: UserListModel?) {
        if (userListModel != null) {
            val list = ArrayList<LocalUser>()
            userListModel.forEach {
                list.add(LocalUser(it.id, it.avatarUrl, it.login))
            }
            listFollowers.postValue(list)
        }
    }


    fun insertListToDb(lifecycleOwner: LifecycleOwner, user: LocalModel) {
        val list: ArrayList<LocalUser> = arrayListOf()

        listFol.observe(lifecycleOwner, Observer {
            if (it != null) {
                it.forEach { user ->
                    list.add(LocalUser(user.id, user.avatarUrl, user.login))
                }
                logDebug("FOLLOWER $list")
                detailRepo.getIsFavorites(user) { isFavorite ->
                    if (!isFavorite) {
                        detailRepo.addFavorites(
                            LocalModel(
                                user.id,
                                user.avatarUrl,
                                user.username,
                                user.name,
                                list,
                                user.followingUrl,
                                user.location
                            )
                        )
                        {
                            logDebug(
                                "FOLLOWER COMPLETE ${LocalModel(
                                    user.id,
                                    user.avatarUrl,
                                    user.username,
                                    user.name,
                                    list,
                                    user.followingUrl,
                                    user.location
                                )}"
                            )
                        }
                    }
                }
            }
        })

    }
}