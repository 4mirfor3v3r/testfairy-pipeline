package com.acemirr.learningpath4sub2.ui.list.datasource

import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import com.acemirr.learningpath4sub2.data.model.User
import com.acemirr.learningpath4sub2.data.repository.ListRepo
import com.acemirr.learningpath4sub2.utils.LoadingState

class ListDataSource(private val listRepo: ListRepo) : PageKeyedDataSource<Int, User>() {

        var state: MutableLiveData<LoadingState> = MutableLiveData()

        override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Int, User>) {
            updateState(LoadingState.LOADING)
            listRepo.getPage(0, params.requestedLoadSize, {
                if (it != null) {
                    callback.onResult(it,null,1)
                }
            },{
                updateState(LoadingState.DONE)
            })
        }

        override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, User>) {
            listRepo.getPage(params.key, params.requestedLoadSize, {
                if (it != null){
                    callback.onResult(it,params.key + 1)
                }
            },{
//                not implemented because confuse loading
            })
        }

        override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, User>) {
        }

        private fun updateState(state: LoadingState) {
            this.state.postValue(state)
        }
    }