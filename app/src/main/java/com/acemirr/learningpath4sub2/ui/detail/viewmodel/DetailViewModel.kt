package com.acemirr.learningpath4sub2.ui.detail.viewmodel

import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.acemirr.learningpath4sub2.data.db.entity.LocalModel
import com.acemirr.learningpath4sub2.data.repository.DetailRepo
import com.acemirr.learningpath4sub2.utils.logDebug

class DetailViewModel(private val detailRepo: DetailRepo) : ViewModel() {
    var isLoading = ObservableField<Boolean>()
    var name = ObservableField<String>()
    var username = ObservableField<String>()
    var location = ObservableField<String>()
    var imageUrl = ObservableField<String>()
    var isLiked = ObservableField<Boolean>()
    var users: LocalModel? = null

    fun getUserDetail(usrname: String) {
        isLoading.set(true)
        detailRepo.getUserDetail(usrname, {
            if (it != null) {
                users = LocalModel(
                    it.id,
                    it.avatarUrl,
                    it.login,
                    it.name,
                    listOf(),
                    listOf(),
                    it.location
                )
                name.set(it.name)
                username.set(it.login)
                location.set(it.location)
                imageUrl.set(it.avatarUrl)
            }
        }, {
            checkIsLiked()
        })
    }

    fun getLocalUserDetail(id: String?) {
        isLoading.set(true)
        detailRepo.getUser(id) {
            name.set(it.name)
            username.set(it.username)
            location.set(it.location)
            imageUrl.set(it.avatarUrl)
            isLoading.set(false)
        }
    }

    private fun checkIsLiked() {
        if (users != null) {
            detailRepo.getIsFavorites(users!!) { inDB ->
                isLiked.set(inDB)
                isLoading.set(false)
            }
        }
    }

    fun onFavClicked() {
        if (users != null)
            onFavoriteClicked(users)
    }

    private fun onFavoriteClicked(user: LocalModel?) {
        if (user != null) {
            detailRepo.getIsFavorites(user) { inDatabase ->
                if (inDatabase) {
                    detailRepo.deleteFavorites(user) {
                        logDebug("DELETING USER $user")
                        isLiked.set(!inDatabase)
                    }
                } else {
                    detailRepo.addFavorites(user) {
                        logDebug("ADDING USER $user ")
                        isLiked.set(!inDatabase)
                    }
                }
            }
        } else {
            logDebug("DETAIL FAV CLICKED")
        }
    }
}