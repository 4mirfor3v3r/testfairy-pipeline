package com.acemirr.learningpath4sub2.ui.base

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.acemirr.learningpath4sub2.data.api.ApiHelper
import com.acemirr.learningpath4sub2.data.repository.DetailRepo
import com.acemirr.learningpath4sub2.data.repository.FavoriteRepo
import com.acemirr.learningpath4sub2.data.repository.ListRepo
import com.acemirr.learningpath4sub2.data.repository.SearchRepo
import com.acemirr.learningpath4sub2.ui.detail.viewmodel.DetailViewModel
import com.acemirr.learningpath4sub2.ui.detail.viewmodel.FragmentDetailFollowerViewModel
import com.acemirr.learningpath4sub2.ui.detail.viewmodel.FragmentDetailFollowingViewModel
import com.acemirr.learningpath4sub2.ui.favorite.viewmodel.FavoriteActivityViewModel
import com.acemirr.learningpath4sub2.ui.list.viewmodel.DialogReminderViewModel
import com.acemirr.learningpath4sub2.ui.list.viewmodel.ListViewModel
import com.acemirr.learningpath4sub2.ui.searchview.viewmodel.SearchActivityViewModel
import kotlinx.coroutines.CoroutineScope

@Suppress("UNCHECKED_CAST")
class ViewModelFactory(
    private val context: Context,
    private val apiHelper: ApiHelper,
    private val scope: CoroutineScope
) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return when {
            modelClass.isAssignableFrom(ListViewModel::class.java) ->
                ListViewModel(ListRepo(context, apiHelper, scope)) as T

            modelClass.isAssignableFrom(DetailViewModel::class.java) ->
                DetailViewModel(DetailRepo(context, apiHelper, scope)) as T

            modelClass.isAssignableFrom(FragmentDetailFollowingViewModel::class.java) ->
                FragmentDetailFollowingViewModel(DetailRepo(context, apiHelper, scope)) as T

            modelClass.isAssignableFrom(FragmentDetailFollowerViewModel::class.java) ->
                FragmentDetailFollowerViewModel(DetailRepo(context, apiHelper, scope)) as T

            modelClass.isAssignableFrom(SearchActivityViewModel::class.java) ->
                SearchActivityViewModel(SearchRepo(context, apiHelper, scope)) as T

            modelClass.isAssignableFrom(FavoriteActivityViewModel::class.java) ->
                FavoriteActivityViewModel(FavoriteRepo(context, scope)) as T

            modelClass.isAssignableFrom(DialogReminderViewModel::class.java) ->
                DialogReminderViewModel() as T

            else -> throw IllegalArgumentException("Unknown Class Name")
        }
    }
}