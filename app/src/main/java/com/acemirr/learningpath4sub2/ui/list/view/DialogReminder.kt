package com.acemirr.learningpath4sub2.ui.list.view

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.acemirr.learningpath4sub2.R
import com.acemirr.learningpath4sub2.data.api.ApiHelper
import com.acemirr.learningpath4sub2.data.api.ApiServiceImpl
import com.acemirr.learningpath4sub2.databinding.DialogReminderBinding
import com.acemirr.learningpath4sub2.ui.base.ViewModelFactory
import com.acemirr.learningpath4sub2.ui.list.viewmodel.DialogReminderViewModel
import com.acemirr.learningpath4sub2.utils.Constant.TIME_PICKER_REPEAT_TAG
import com.acemirr.learningpath4sub2.utils.appwidget.TimePickerFragment


class DialogReminder : Fragment(), TimePickerFragment.DialogTimeListener {
    private lateinit var viewModel: DialogReminderViewModel
    lateinit var binding: DialogReminderBinding
    private lateinit var ctx: Context
    private lateinit var listActivity: ListActivity
    private val timePickerFragmentRepeat = TimePickerFragment()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_reminder, container, false)
        binding.lifecycleOwner = this

        ctx = (context as ListActivity)
        listActivity = (activity as ListActivity)

        listActivity.setDialogListener(this)

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel = ViewModelProvider(this, ViewModelFactory(ctx, ApiHelper(ApiServiceImpl()), lifecycleScope))
            .get(DialogReminderViewModel::class.java)
        binding.vm = viewModel

        timePickerFragmentRepeat.onAttach(ctx)

        viewModel.getPrefValue()
        handleClickEvent()
    }

    private fun handleClickEvent() {
        binding.btnDialogReminderTime.setOnClickListener {
            timePickerFragmentRepeat.show(childFragmentManager, TIME_PICKER_REPEAT_TAG)
        }
        binding.switchDialogReminder.setOnCheckedChangeListener { _, isChecked ->
            viewModel.onSwitchChange(isChecked)
        }
        binding.btnDialogReminderSave.setOnClickListener {
            viewModel.onBtnSaveClicked(ctx)
            listActivity.setLayoutHidden(false)
        }
    }

    override fun onDialogTimeSet(tag: String?, hourOfDay: Int, minute: Int) {
        viewModel.onTimeSet(tag, hourOfDay, minute)

    }
}
