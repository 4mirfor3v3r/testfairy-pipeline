package com.acemirr.learningpath4sub2.ui.searchview.view

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.acemirr.learningpath4sub2.R
import com.acemirr.learningpath4sub2.data.api.ApiHelper
import com.acemirr.learningpath4sub2.data.api.ApiServiceImpl
import com.acemirr.learningpath4sub2.data.model.User
import com.acemirr.learningpath4sub2.databinding.ActivitySearchBinding
import com.acemirr.learningpath4sub2.listener.PagingOnItemClick
import com.acemirr.learningpath4sub2.ui.base.ViewModelFactory
import com.acemirr.learningpath4sub2.ui.detail.view.DetailActivity
import com.acemirr.learningpath4sub2.ui.searchview.adapter.SearchRvAdapter
import com.acemirr.learningpath4sub2.ui.searchview.viewmodel.SearchActivityViewModel
import com.acemirr.learningpath4sub2.utils.Constant
import com.acemirr.learningpath4sub2.utils.DebouncingTextListener
import com.acemirr.learningpath4sub2.utils.logError

class SearchActivity : AppCompatActivity() {
    private lateinit var binding: ActivitySearchBinding
    private lateinit var viewModel: SearchActivityViewModel
    var adapter = SearchRvAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_search)
        viewModel = ViewModelProvider(
            this,
            ViewModelFactory(this, ApiHelper(ApiServiceImpl()), lifecycleScope)
        ).get(SearchActivityViewModel::class.java)
        binding.vm = viewModel

        setupSearch()
        setupRecyclerView()
    }

    override fun onStart() {
        super.onStart()
        observeData()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(R.anim.slide_from_bottom, R.anim.slide_to_top)
    }

    private fun setupSearch() {
        binding.tilSearch.editText?.requestFocus()
        binding.tilSearch.editText?.addTextChangedListener(DebouncingTextListener(this@SearchActivity.lifecycle) {
            if (it != null) {
                viewModel.getAllUsers(it)
            }
        })
    }

    private fun setupRecyclerView() {
        val layoutManager = LinearLayoutManager(this)
        binding.rvSearch.layoutManager = layoutManager
        adapter.setOnItemClick(object : PagingOnItemClick {
            override fun onItemClicked(view: View, user: User) {
                onItemClicked(user)
            }

        })
        binding.rvSearch.adapter = adapter
    }

    private fun observeData() {
        viewModel.searchModel.observe(this, Observer {
            adapter.submitList(it.items)
            logError("SEARCH SUBMIT ${it.items}")
        })
    }

    private fun onItemClicked(pagingModel: User) {
        val intent = Intent(this, DetailActivity::class.java)
        intent.putExtra(Constant.EXTRA_DATA_PAGING, pagingModel)
        startActivity(intent)
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left)
    }
}
