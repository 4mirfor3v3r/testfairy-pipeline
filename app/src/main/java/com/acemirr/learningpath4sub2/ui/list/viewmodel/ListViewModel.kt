package com.acemirr.learningpath4sub2.ui.list.viewmodel

import androidx.databinding.ObservableField
import androidx.lifecycle.*
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.acemirr.learningpath4sub2.data.model.User
import com.acemirr.learningpath4sub2.data.repository.ListRepo
import com.acemirr.learningpath4sub2.ui.list.datasource.ListDataSource
import com.acemirr.learningpath4sub2.ui.list.datasource.ListDataSourceFactory
import com.acemirr.learningpath4sub2.utils.LoadingState

class ListViewModel(listRepo: ListRepo) : ViewModel() {
    val isLoading = ObservableField<Boolean>()
    val isSuppressed = ObservableField<Boolean>()

    private var pagingDataSourceFactory: ListDataSourceFactory = ListDataSourceFactory(listRepo)
    var pagingList: LiveData<PagedList<User>>

    init {
        val config = PagedList.Config.Builder()
            .setPageSize(30)
            .setInitialLoadSizeHint(40)
            .setEnablePlaceholders(false)
            .build()
        pagingList = LivePagedListBuilder(pagingDataSourceFactory, config).build()
    }
    fun getLoadingState(lifecycleOwner: LifecycleOwner) {
        val liveIsLoading = Transformations.switchMap(
            pagingDataSourceFactory.listDataSourceLiveData,
            ListDataSource::state
        )
        liveIsLoading.observe(lifecycleOwner, Observer {
            if (it == LoadingState.DONE)
                isLoading.set(false)
            else
                isLoading.set(true)
        })
    }
}