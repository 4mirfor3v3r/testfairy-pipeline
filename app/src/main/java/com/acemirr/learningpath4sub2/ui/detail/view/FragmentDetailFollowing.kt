package com.acemirr.learningpath4sub2.ui.detail.view

import android.content.Context
import android.content.res.Configuration
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.acemirr.learningpath4sub2.R
import com.acemirr.learningpath4sub2.data.api.ApiHelper
import com.acemirr.learningpath4sub2.data.api.ApiServiceImpl
import com.acemirr.learningpath4sub2.data.db.entity.LocalModel
import com.acemirr.learningpath4sub2.databinding.FragmentDetailFollowingBinding
import com.acemirr.learningpath4sub2.listener.FollowingCallback
import com.acemirr.learningpath4sub2.ui.base.ViewModelFactory
import com.acemirr.learningpath4sub2.ui.detail.adapter.DetailFragmentFollowingRvAdapter
import com.acemirr.learningpath4sub2.ui.detail.viewmodel.FragmentDetailFollowingViewModel

class FragmentDetailFollowing : Fragment(), FollowingCallback {
    private lateinit var viewModel: FragmentDetailFollowingViewModel
    lateinit var binding: FragmentDetailFollowingBinding
    private lateinit var adapter: DetailFragmentFollowingRvAdapter
    private lateinit var ctx: Context
    private lateinit var detailActivity: DetailActivity

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_detail_following, container, false)
        binding.lifecycleOwner = this
        ctx = (context as DetailActivity)
        detailActivity = (activity as DetailActivity)

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(
            this,
            ViewModelFactory(ctx, ApiHelper(ApiServiceImpl()), lifecycleScope)
        )
            .get(FragmentDetailFollowingViewModel::class.java)
        binding.vm = viewModel

        detailActivity.onFollowingCallback(this)

        setupRecyclerView()
        observeData()

        detailActivity.extraUser?.login?.let { viewModel.getFollowing(it) }
        detailActivity.extraLocalModel?.followersUrl?.let {
            viewModel.listFollowing.postValue(it)
        }
    }

    override fun onResume() {
        super.onResume()
        binding.rvDetailFragFollowing.startLayoutAnimation()
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        activity?.recreate()
    }

    private fun observeData() {
        viewModel.listFollowing.observe(viewLifecycleOwner, Observer {
            adapter.submitList(it)
            binding.rvDetailFragFollowing.startLayoutAnimation()
        })
    }

    private fun setupRecyclerView() {
        val layoutManager = LinearLayoutManager(context)
        binding.rvDetailFragFollowing.layoutManager = layoutManager

        adapter = DetailFragmentFollowingRvAdapter()

        binding.rvDetailFragFollowing.adapter = adapter
    }

    override fun onLikesAdded(localModel: LocalModel) {
        try {
            viewModel.insertListToDb(viewLifecycleOwner, localModel)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

}
