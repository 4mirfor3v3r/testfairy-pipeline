package com.acemirr.learningpath4sub2.utils

import android.content.Context
import android.util.Log
import android.widget.Toast
import androidx.room.TypeConverter
import com.acemirr.learningpath4sub2.BuildConfig
import com.acemirr.learningpath4sub2.data.db.entity.LocalUser
import com.acemirr.learningpath4sub2.utils.Constant.TAG_DEBUG
import com.acemirr.learningpath4sub2.utils.Constant.TAG_ERROR
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.util.*

fun logDebug(message: String) {
    if (BuildConfig.DEBUG) Log.d(TAG_DEBUG, message)
}

fun logError(message: String) {
    if (BuildConfig.DEBUG) Log.e(TAG_ERROR, message)
}

fun Context.showToast(message: String){
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}
class LocalUserTypeConverter {
    private val gson = Gson()

    @TypeConverter
    fun stringToList(data: String?): List<LocalUser> {
        if (data == null) { return Collections.emptyList() }
        val listType = object : TypeToken<List<LocalUser>>() {}.type
        return gson.fromJson(data, listType)
    }

    @TypeConverter
    fun listToString(someObjects: List<LocalUser>?): String {
        return gson.toJson(someObjects)
    }
}