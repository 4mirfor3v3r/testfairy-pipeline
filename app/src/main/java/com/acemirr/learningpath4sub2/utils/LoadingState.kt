package com.acemirr.learningpath4sub2.utils

enum class LoadingState {
    DONE, LOADING
}