package com.acemirr.learningpath4sub2.utils

object Constant {
    const val PREFERENCE_NAME = "com.acemirr.learningpath4sub2.pref"
    const val PREF_TIME_KEY = "pref_time"
    const val PREF_STATE_KEY = "pref_state"

    const val TAG_DEBUG = "TAG_DEBUG"
    const val TAG_ERROR = "TAG_ERROR"

    const val TIME_OUT = 30L

    const val EXTRA_DATA_PAGING = "datapaging"
    const val EXTRA_LOCAL_DATA = "localdata"

    const val TIME_PICKER_REPEAT_TAG = "TimePickerRepeat"


    const val WIDGET_CLICK = "com.acemirr.learningpath4sub2.WIDGET_CLICK"
    const val WIDGET_ID_EXTRA = "com.acemirr.learningpath4sub2.WIDGET_EXTRA"
    const val WIDGET_LOCAL_EXTRA = "com.acemirr.learningpath4sub2.WIDGET_LOCAL_EXTRA"
    const val WIDGET_USERNAME_EXTRA = "com.acemirr.learningpath4sub2.WIDGET_USERNAME_EXTRA"

    const val FAVORITE_TABLE_NAME = "data"
    const val AUTHORITY = "com.acemirr.learningpath4sub2"

    /**
     * The match code for some items in the Person table
     */
    const val ID_PERSON_DATA = 1

    /**
     * The match code for an item in the PErson table
     */
    const val ID_PERSON_DATA_ITEM = 2

    const val TYPE_ONE_TIME = "OneTimeAlarm"
    const val TYPE_REPEATING = "Awesome Git App"
    const val EXTRA_MESSAGE = "message"
    const val EXTRA_TYPE = "type"

    // Siapkan 2 id untuk 2 macam alarm, onetime dan repeating
    const val ID_ONETIME = 100
    const val ID_REPEATING = 101

    const val DATE_FORMAT = "yyyy-MM-dd"
    const val TIME_FORMAT = "HH:mm"

}