package com.acemirr.learningpath4sub2.utils

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.view.View
import android.view.animation.OvershootInterpolator
import android.widget.ImageView
import androidx.appcompat.widget.AppCompatImageView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.acemirr.learningpath4sub2.R
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.squareup.picasso.Picasso
import com.squareup.picasso.RequestCreator

object BindingAdapters {
    private val picasso: Picasso
        get() = Picasso.get()

    private fun ImageView.load(path: String, request: (RequestCreator) -> RequestCreator) {
        picasso.setIndicatorsEnabled(true)
        request(picasso.load(path)).into(this)
    }

    @JvmStatic
    @BindingAdapter("imageUrl")
    fun loadImage(view: ImageView, url: String?) {
        if (url != null) {
            view
                .load(url)
                { requestCreator ->
                    requestCreator.fit().centerCrop()
                }
        }
    }

    @JvmStatic
    @BindingAdapter("suppress")
    fun suppress(view: RecyclerView, isSuppressed: Boolean) {
        view.isEnabled = !isSuppressed
        view.isClickable = !isSuppressed
        view.suppressLayout(isSuppressed)
    }

    @JvmStatic
    @BindingAdapter("hideAnimate")
    fun scaleView(view: FloatingActionButton, isHidden: Boolean) {
        view.isEnabled = !isHidden
        if (isHidden) {
            animateScale(view, 800, 1f, 0f)
        } else {
            animateScale(view, 800, 0f, 1f)
        }
    }

    @JvmStatic
    @BindingAdapter("animateIcon")
    fun animateIcon(view: AppCompatImageView, isLiked: Boolean?) {
        animateScale(view, 300, 0.2f, 1f)
        if (isLiked != null) {
            if (isLiked) {
                view.setImageResource(R.drawable.ic_favorite_filled)
            } else {
                view.setImageResource(R.drawable.ic_favorite_outline)
            }
        }
    }

    private fun animateScale(view: View, duration: Long, valueStart: Float, valueEnd: Float) {
        val overshoot = OvershootInterpolator(4f)
        val animatorSet = AnimatorSet()
        val bounceAnimX = ObjectAnimator.ofFloat(view, "scaleX", valueStart, valueEnd)
        bounceAnimX.duration = duration
        bounceAnimX.interpolator = overshoot
        val bounceAnimY = ObjectAnimator.ofFloat(view, "scaleY", valueStart, valueEnd)
        bounceAnimX.duration = duration
        bounceAnimX.interpolator = overshoot
        animatorSet.play(bounceAnimX).with(bounceAnimY)
        animatorSet.start()
    }
}