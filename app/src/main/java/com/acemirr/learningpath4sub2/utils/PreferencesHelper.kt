package com.acemirr.learningpath4sub2.utils

import android.content.Context
import android.content.SharedPreferences

class PreferencesHelper(context: Context) {

    private val prefHelper: SharedPreferences = context.getSharedPreferences(Constant.PREFERENCE_NAME, Context.MODE_PRIVATE)

    fun setString(key: String, value: String){
        prefHelper.edit().putString(key, value).apply()
        logDebug("PREF STRING INSERTED KEY:$key VALUE:$value")
    }

    fun getString(key: String): String{
        logDebug("PREF STRING FETCHED KEY:$key")
        return prefHelper.getString(key, "")!!
    }
}