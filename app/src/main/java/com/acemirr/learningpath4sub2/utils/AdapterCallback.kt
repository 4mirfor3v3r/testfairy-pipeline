package com.acemirr.learningpath4sub2.utils

import androidx.recyclerview.widget.DiffUtil
import com.acemirr.learningpath4sub2.data.db.entity.LocalModel
import com.acemirr.learningpath4sub2.data.db.entity.LocalUser
import com.acemirr.learningpath4sub2.data.model.User

class AdapterCallback {
    companion object{
        /**
         * diff callback adapter list
         */
        val DiffUserCallback = object : DiffUtil.ItemCallback<User>() {
            override fun areItemsTheSame(oldItem: User, newItem: User): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(oldItem: User, newItem: User): Boolean {
                return oldItem.id == newItem.id
            }
        }
        val DiffLocalUserCallback = object : DiffUtil.ItemCallback<LocalUser>() {
            override fun areItemsTheSame(oldItem: LocalUser, newItem: LocalUser): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(oldItem: LocalUser, newItem: LocalUser): Boolean {
                return oldItem.id == newItem.id
            }
        }

        val DiffLocalmodelCallback = object : DiffUtil.ItemCallback<LocalModel>() {
            override fun areItemsTheSame(oldItem: LocalModel, newItem: LocalModel): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(oldItem: LocalModel, newItem: LocalModel): Boolean {
                return oldItem.id == newItem.id
            }

        }
    }
}