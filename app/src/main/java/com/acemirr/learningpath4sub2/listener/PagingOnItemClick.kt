package com.acemirr.learningpath4sub2.listener

import android.view.View
import com.acemirr.learningpath4sub2.data.model.User

interface PagingOnItemClick {
    fun onItemClicked(view: View, user: User)
}