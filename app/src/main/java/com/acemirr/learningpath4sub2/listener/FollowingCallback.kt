package com.acemirr.learningpath4sub2.listener

import com.acemirr.learningpath4sub2.data.db.entity.LocalModel

interface FollowingCallback {
    fun onLikesAdded(localModel: LocalModel)
}

interface FollowerCallback {
    fun onLikesAdded(localModel: LocalModel)
}