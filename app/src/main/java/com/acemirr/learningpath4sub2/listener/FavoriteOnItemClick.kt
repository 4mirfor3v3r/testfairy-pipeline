package com.acemirr.learningpath4sub2.listener

import android.view.View
import com.acemirr.learningpath4sub2.data.db.entity.LocalModel

interface FavoriteOnItemClick {
    fun onItemClicked(view: View, user: LocalModel)
    fun onLongClicked(view: View, user: LocalModel): Boolean
    fun onTrashClicked(view: View, user: LocalModel)
}