package com.acemirr.learningpath4sub2.data.model


import com.google.gson.annotations.SerializedName

data class DetailUserModel(
    @SerializedName("avatar_url")
    var avatarUrl: String?,
    @SerializedName("followers")
    var followers: Int?,
    @SerializedName("followers_url")
    var followersUrl: String?,
    @SerializedName("following")
    var following: Int?,
    @SerializedName("following_url")
    var followingUrl: String?,
    @SerializedName("id")
    var id: Int?,
    @SerializedName("location")
    var location: String?,
    @SerializedName("login")
    var login: String?,
    @SerializedName("name")
    var name: String?,
    @SerializedName("repos_url")
    var reposUrl: String?
)