package com.acemirr.learningpath4sub2.data.repository

import android.content.Context
import com.acemirr.learningpath4sub2.data.api.ApiHelper
import com.acemirr.learningpath4sub2.data.api.Network
import com.acemirr.learningpath4sub2.data.model.SearchModel
import kotlinx.coroutines.CoroutineScope

class SearchRepo(private val context: Context, private val apiHelper: ApiHelper, coroutineScope: CoroutineScope) {
    private val network = Network(coroutineScope)
    fun searchUser(username: String, onSuccess: (SearchModel?) -> Unit, onFinally: (Boolean) -> Unit) {
        network.request(context, {
            apiHelper.searchUser(username)
        }, {
            onSuccess(it)
        }, {
            onFinally(it)
        })
    }
}