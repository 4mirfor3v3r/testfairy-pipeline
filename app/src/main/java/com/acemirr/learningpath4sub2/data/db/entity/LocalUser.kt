package com.acemirr.learningpath4sub2.data.db.entity

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "users")
data class LocalUser(
    @PrimaryKey
    @ColumnInfo(name = "id")
    var id: Int,
    @ColumnInfo(name = "avatar_url")
    var avatarUrl: String,
    @ColumnInfo(name = "login")
    var login: String
) : Parcelable
