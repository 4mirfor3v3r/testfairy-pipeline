package com.acemirr.learningpath4sub2.data.db.entity

import android.content.ContentValues
import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.acemirr.learningpath4sub2.utils.LocalUserTypeConverter
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "data")
data class LocalModel(
    @PrimaryKey
    @ColumnInfo(name = "id")
    var id: Int?,
    @ColumnInfo(name = "avatar_url")
    var avatarUrl: String?,
    @ColumnInfo(name = "username")
    var username: String?,
    @ColumnInfo(name = "name")
    var name: String?,

    @TypeConverters(LocalUserTypeConverter::class)
    @SerializedName("followers")
    var followersUrl: List<LocalUser>,

    @TypeConverters(LocalUserTypeConverter::class)
    @SerializedName("following")
    var followingUrl: List<LocalUser>,

    @ColumnInfo(name = "location")
    var location: String?
) : Parcelable{

    companion object {
        fun fromContentValues(values: ContentValues): LocalModel {
            val localModel = LocalModel(null,null,null, null, listOf(), listOf(),null)
            if (values.containsKey("id")) localModel.id = values.getAsInteger("id")
            if (values.containsKey("avatarUrl")) localModel.avatarUrl = values.getAsString("avatarUrl")
            if (values.containsKey("username")) localModel.username=values.getAsString("username")
            if (values.containsKey("name")) localModel.name = values.getAsString("name")
            if (values.containsKey("location")) localModel.location = values.getAsString("location")
            return localModel
        }
    }
}