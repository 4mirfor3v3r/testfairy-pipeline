package com.acemirr.learningpath4sub2.data.api

import android.content.Context
import com.acemirr.learningpath4sub2.R
import com.acemirr.learningpath4sub2.utils.logDebug
import com.acemirr.learningpath4sub2.utils.logError
import com.acemirr.learningpath4sub2.utils.showToast
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

class Network(private val coroutineScope: CoroutineScope) {

    fun <T> request(
        context: Context,
        response: suspend () -> retrofit2.Response<T>,
        onSuccess: (T?) -> Unit,
        onFinally: (Boolean) -> Unit
    ) {
        coroutineScope.launch {
            try {
                val result = response()
                if (result.isSuccessful) {
                    logDebug("Network # isSuccessful")
                    logDebug("Network # url API : ${result.raw().request.url}")
                    onSuccess(result.body())
                } else {
                    logError("Network # UnSuccessFul")
                    logError("Network # url API : ${result.raw().request.url}")
                    logError("Network # code : ${result.code()}")
                    logError("Network # body : ${result.errorBody().toString()}")
                    if (result.code() == 403){
                        context.showToast(context.getString(R.string.network_limit_reached))
                    }else {
                        context.showToast(String.format(context.getString(R.string.network_error_code),result.code()))
                    }
                }
            } catch (throwable: Throwable) {
                if (throwable is CancellationException) {
                    // coroutines has canceled
                    logError("Network # Throwable ==> job canceled")
                } else {
                    logError("Network # Throwable")
                    context.showToast(String.format(context.getString(R.string.network_error_throwable),throwable.message))
                    throwable.printStackTrace()
                }
            } finally {
                logDebug("Network # finally")
                onFinally(true)
            }
        }
    }

}