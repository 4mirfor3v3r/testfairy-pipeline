package com.acemirr.learningpath4sub2.data.repository

import android.content.Context
import androidx.lifecycle.MutableLiveData
import com.acemirr.learningpath4sub2.data.db.UserDatabase
import com.acemirr.learningpath4sub2.data.db.entity.LocalModel
import com.acemirr.learningpath4sub2.utils.LoadingState
import com.acemirr.learningpath4sub2.utils.logDebug
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class FavoriteRepo(context: Context, private val coroutineScope: CoroutineScope) {

    private val db = UserDatabase(context)
    var loadingState: MutableLiveData<LoadingState> = MutableLiveData()

    fun deleteFavorites(user: LocalModel, result: (Int) -> Unit) {
        coroutineScope.launch {
            val a = async { db.getUserDao().deleteLocal(user) }
            val res = a.await()
            if (a.isCompleted) {
                result(res)
                logDebug("LOCAL USER DELETED $res")
            }
        }
    }

    fun getLocalData(list: (List<LocalModel>) -> Unit) {
        loadingState.postValue(LoadingState.LOADING)
        coroutineScope.launch {
            delay(400)
            val a = async { db.getUserDao().getAllUserData() }
            val listLocalUser = a.await()
            if (a.isCompleted) {
                list(listLocalUser)
                loadingState.postValue(LoadingState.DONE)
                logDebug("LOCAL USER FETCHED $listLocalUser")
            }
        }
    }

}