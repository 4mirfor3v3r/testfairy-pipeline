package com.acemirr.learningpath4sub2.data.model


import com.google.gson.annotations.SerializedName

data class SearchModel(
    @SerializedName("incomplete_results")
    var incompleteResults: Boolean,
    @SerializedName("items")
    var items: List<User>,
    @SerializedName("total_count")
    var totalCount: Int
)