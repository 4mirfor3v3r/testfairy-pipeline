package com.acemirr.learningpath4sub2.data.repository

import android.content.Context
import com.acemirr.learningpath4sub2.data.api.ApiHelper
import com.acemirr.learningpath4sub2.data.api.Network
import com.acemirr.learningpath4sub2.data.model.PagingModel
import kotlinx.coroutines.CoroutineScope

class ListRepo(
    private val context: Context,
    private val apiHelper: ApiHelper,
    coroutineScope: CoroutineScope
) {
    private val network = Network(coroutineScope)

    fun getPage(
        page: Int,
        pageSize: Int,
        onSuccess: (PagingModel?) -> Unit,
        onFinally: (Boolean) -> Unit
    ) {
        network.request(context, {
            apiHelper.getPagedUsers(page, pageSize)
        }, {
            onSuccess(it)
        }, {
            onFinally(it)
        })
    }
}