package com.acemirr.learningpath4sub2.data.db

import android.database.Cursor
import androidx.room.*
import com.acemirr.learningpath4sub2.data.db.entity.LocalModel
import com.acemirr.learningpath4sub2.data.db.entity.LocalUser


@Dao
interface UserDao {

    /*    <==================================== LOCAL USER =========================================> */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun upsertUser(user: LocalUser)

    @Delete
    suspend fun deleteUser(user: LocalUser)

    @Query("SELECT * FROM users")
    suspend fun getAllUser(): List<LocalUser>


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(user: LocalModel): Long

    @Query("SELECT * FROM users WHERE id LIKE :id")
    suspend fun getOne(id: Int): LocalUser

/*    <=================================== LOCAL MODEL =========================================> */

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun upsertLocal(user: LocalModel)

    @Delete
    suspend fun deleteLocal(user: LocalModel): Int

    @Query("SELECT * FROM data WHERE id LIKE :id")
    suspend fun getOneData(id: Int): LocalModel

    @Query("SELECT * FROM data WHERE username LIKE :username")
    suspend fun getOneData(username: String): LocalModel

    @Query("SELECT * FROM data")
    suspend fun getAllUserData(): List<LocalModel>

    @Query("SELECT * FROM data")
    fun getAllUserDataWidget(): List<LocalModel>

    @Query("SELECT * FROM data")
    fun getAllUserAsCursor(): Cursor

}