package com.acemirr.learningpath4sub2.data.repository

import android.content.Context
import com.acemirr.learningpath4sub2.data.api.ApiHelper
import com.acemirr.learningpath4sub2.data.api.Network
import com.acemirr.learningpath4sub2.data.db.UserDatabase
import com.acemirr.learningpath4sub2.data.db.entity.LocalModel
import com.acemirr.learningpath4sub2.data.model.DetailUserModel
import com.acemirr.learningpath4sub2.data.model.UserListModel
import com.acemirr.learningpath4sub2.utils.logDebug
import kotlinx.coroutines.*

class DetailRepo(
    private val context: Context,
    private val apiHelper: ApiHelper,
    private val coroutineScope: CoroutineScope
) {
    private val network = Network(coroutineScope)
    private val db = UserDatabase(context)

    fun getUserDetail(
        username: String,
        onSuccess: (DetailUserModel?) -> Unit,
        onFinally: (Boolean) -> Unit
    ) {
        network.request(context,{
            apiHelper.getUserDetail(username)
        },{
            onSuccess(it)
        },{
            onFinally(it)
        })
    }

    fun getFollowers(
        username: String,
        onSuccess: (UserListModel?) -> Unit,
        onFinally: (Boolean) -> Unit
    ) {
        network.request(context,{
            apiHelper.getFollowers(username)
        },{
            onSuccess(it)
        },{
            onFinally(it)
        })
    }

    fun getFollowing(
        username: String,
        onSuccess: (UserListModel?) -> Unit,
        onFinally: (Boolean) -> Unit
    ) {
        network.request(context,{
            apiHelper.getFollowing(username)
        },{
            onSuccess(it)
        },{
            onFinally(it)
        })
    }

    fun getUser(username: String?, result: (LocalModel) -> Unit) {
        coroutineScope.launch {
            delay(1000)
            val defUser = async { username?.let { db.getUserDao().getOneData(it) } }
            val res = defUser.await()
            if (defUser.isCompleted) {
                if (res != null) {
                    result(res)
                    logDebug("RESULT MODEL $res")
                }
            }
        }
    }

    fun getIsFavorites(user: LocalModel, result: (Boolean) -> Unit) {
        var defUser: Deferred<LocalModel?>
        coroutineScope.launch {
            defUser = async { user.id?.let { db.getUserDao().getOneData(it) } }
            val res = defUser.await()
            if (res != null)
                result(true)
            else
                result(false)
//            logDebug("IS FAVORITES $user")
        }
    }

    fun addFavorites(user: LocalModel, isCompleted: (Boolean) -> Unit) {
        coroutineScope.launch {
            val a = async {
                db.getUserDao().upsertLocal(user)
            }
            if (a.isCompleted) {
                isCompleted(true)
            } else {
                isCompleted(false)
            }
//            logDebug("IS ADDED $user")
        }
    }

    fun deleteFavorites(user: LocalModel, isCompleted: (Boolean) -> Unit) {
        coroutineScope.launch {
            val a = async {
                db.getUserDao().deleteLocal(user)
            }
            if (a.isCompleted) {
                isCompleted(true)
            } else {
                isCompleted(false)
            }
//            logDebug("IS DELETED $user")
        }
    }
}