package com.acemirr.learningpath4sub2.data.api

class ApiServiceImpl : ApiService {
    override suspend fun getUserDetail(userID: String?) = NetworkConfig.api().getUserDetail(userID)

    override suspend fun searchUser(userID: String) = NetworkConfig.api().searchUser(userID)

    override suspend fun getFollowers(userID: String) = NetworkConfig.api().getFollowers(userID)

    override suspend fun getFollowing(userID: String) = NetworkConfig.api().getFollowing(userID)

    override suspend fun getPagedUsers(page: Int, pageSize: Int) =
        NetworkConfig.api().getPagedUsers(page, pageSize)
}