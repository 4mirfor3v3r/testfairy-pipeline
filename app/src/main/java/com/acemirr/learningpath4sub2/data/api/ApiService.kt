package com.acemirr.learningpath4sub2.data.api

import com.acemirr.learningpath4sub2.data.model.DetailUserModel
import com.acemirr.learningpath4sub2.data.model.PagingModel
import com.acemirr.learningpath4sub2.data.model.SearchModel
import com.acemirr.learningpath4sub2.data.model.UserListModel
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {
    @GET("users/{username}")
    @Headers("Authorization: token f30f7cafbeecbc64b92297cfd4886b191e51faab")
    suspend fun getUserDetail(@Path("username") userID: String?): Response<DetailUserModel>

    @GET("search/users")
    @Headers("Authorization: token f30f7cafbeecbc64b92297cfd4886b191e51faab")
    suspend fun searchUser(@Query("q") userID: String): Response<SearchModel>

    @GET("users/{username}/followers")
    @Headers("Authorization: token f30f7cafbeecbc64b92297cfd4886b191e51faab")
    suspend fun getFollowers(@Path("username") userID: String): Response<UserListModel>

    @GET("users/{username}/following")
    @Headers("Authorization: token f30f7cafbeecbc64b92297cfd4886b191e51faab")
    suspend fun getFollowing(@Path("username") userID: String): Response<UserListModel>

    @GET("users")
    @Headers("Authorization: token f30f7cafbeecbc64b92297cfd4886b191e51faab")
    suspend fun getPagedUsers(
        @Query("since") page: Int,
        @Query("per_page") pageSize: Int
    ): Response<PagingModel>
}