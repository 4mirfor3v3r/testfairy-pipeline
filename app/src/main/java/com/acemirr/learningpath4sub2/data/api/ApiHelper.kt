package com.acemirr.learningpath4sub2.data.api

class ApiHelper(private val apiService: ApiService) {
    suspend fun getUserDetail(username: String?) = apiService.getUserDetail(username)

    suspend fun searchUser(query: String) = apiService.searchUser(query)

    suspend fun getFollowers(username: String) = apiService.getFollowers(username)

    suspend fun getFollowing(username: String) = apiService.getFollowing(username)

    suspend fun getPagedUsers(page: Int, pageSize: Int) = apiService.getPagedUsers(page, pageSize)
}