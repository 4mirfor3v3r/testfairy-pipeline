package com.acemirr.learningpath4sub2.services.provider

import android.content.ContentProvider
import android.content.ContentUris
import android.content.ContentValues
import android.content.UriMatcher
import android.database.Cursor
import android.net.Uri
import com.acemirr.learningpath4sub2.data.db.UserDao
import com.acemirr.learningpath4sub2.data.db.UserDatabase
import com.acemirr.learningpath4sub2.data.db.entity.LocalModel
import com.acemirr.learningpath4sub2.utils.Constant.AUTHORITY
import com.acemirr.learningpath4sub2.utils.Constant.FAVORITE_TABLE_NAME
import com.acemirr.learningpath4sub2.utils.Constant.ID_PERSON_DATA
import com.acemirr.learningpath4sub2.utils.Constant.ID_PERSON_DATA_ITEM


class FavoritesContentProvider : ContentProvider() {
    private lateinit var userDao: UserDao
    private val uriMatcher = UriMatcher(UriMatcher.NO_MATCH)

    init {
        uriMatcher.addURI(
            AUTHORITY,
            FAVORITE_TABLE_NAME,
            ID_PERSON_DATA
        )
        uriMatcher.addURI(
            AUTHORITY,
            FAVORITE_TABLE_NAME +
            "/*", ID_PERSON_DATA_ITEM
        )
    }

    override fun delete(uri: Uri, selection: String?, selectionArgs: Array<String>?): Int {
        return -1
    }

    override fun getType(uri: Uri): String? {
        return null
    }

    override fun insert(uri: Uri, values: ContentValues?): Uri? {
        when (uriMatcher.match(uri)) {
            ID_PERSON_DATA -> {
                if (context != null) {
                    val id: Long? = values?.let { LocalModel.fromContentValues(it) }?.let {
                        userDao.insert(
                            it
                        )
                    }
                    if (id != 0L) {
                        context!!.contentResolver
                            .notifyChange(uri, null)
                        return id?.let { ContentUris.withAppendedId(uri, it) }
                    }
                }
                throw java.lang.IllegalArgumentException("Invalid URI: Insert failed$uri")
            }
            ID_PERSON_DATA_ITEM -> throw java.lang.IllegalArgumentException("Invalid URI: Insert failed$uri")
            else -> throw java.lang.IllegalArgumentException("Unknown URI: $uri")
        }
    }

    override fun onCreate(): Boolean {
        userDao = UserDatabase(context!!).getUserDao()
        return false
    }

    override fun query(uri: Uri,projection: Array<String>?, selection: String?, selectionArgs: Array<String>?, sortOrder: String?): Cursor? {
        val cursor: Cursor?
        when (uriMatcher.match(uri)) {
            ID_PERSON_DATA -> {
                cursor = userDao.getAllUserAsCursor()
                if (context != null) {
                    cursor.setNotificationUri(context!!.contentResolver, uri)
                    return cursor
                }
                throw IllegalArgumentException("Unknown URI: $uri")
            }
            else -> throw IllegalArgumentException("Unknown URI: $uri")
        }
    }

    override fun update(uri: Uri, values: ContentValues?, selection: String?, selectionArgs: Array<String>?): Int {
        return -1
    }
}
