package com.acemirr.consumerapp.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Model(
    var id:Int,
    var avatarUrl: String?,
    var username: String?,
    var name: String?,
    var location:String?
):Parcelable