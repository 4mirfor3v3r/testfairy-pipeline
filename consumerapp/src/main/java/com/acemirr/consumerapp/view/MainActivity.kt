package com.acemirr.consumerapp.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.acemirr.consumerapp.R
import com.acemirr.consumerapp.databinding.ActivityMainBinding
import com.acemirr.consumerapp.viewmodel.MainActivityViewModel

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var viewModel:MainActivityViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        viewModel = ViewModelProvider(this).get(MainActivityViewModel::class.java)
        binding.vm = viewModel

        setupSwipeRefreshLayout()
    }

    override fun onStart() {
        super.onStart()
        viewModel.getData(this)
    }

    private fun setupSwipeRefreshLayout(){
        binding.srlConsumerMain.setOnRefreshListener {
            viewModel.getData(this)
        }
    }
}
