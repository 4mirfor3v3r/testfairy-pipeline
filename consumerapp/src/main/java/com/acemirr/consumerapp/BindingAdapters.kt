package com.acemirr.consumerapp

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.acemirr.consumerapp.adapter.MainRvAdapter
import com.acemirr.consumerapp.model.Model
import com.squareup.picasso.Picasso
import com.squareup.picasso.RequestCreator

object BindingAdapters {
    private val picasso: Picasso
        get() = Picasso.get()

    private fun ImageView.load(path: String, request: (RequestCreator) -> RequestCreator) {
        picasso.setIndicatorsEnabled(true)
        request(
            picasso
                .load(path)
        )
            .into(this)
    }

    @JvmStatic
    @BindingAdapter("imageUrl")
    fun loadImage(view: ImageView, url: String?) {
        if (url != null) {
            view
                .load(url)
                { requestCreator ->
                    requestCreator.fit().centerCrop()
                }
        }
    }

    @JvmStatic
    @BindingAdapter("mainList")
    fun setMainList(view:RecyclerView,list:ArrayList<Model>){
        if (view.adapter == null) {
            val adapter = MainRvAdapter()
            view.adapter = adapter
            adapter.apply {
                this.submitList(list)
            }
        }else{
            (view.adapter as MainRvAdapter).apply {
                this.submitList(list)
            }
        }
    }

    @JvmStatic
    @BindingAdapter("setRefresh")
    fun SwipeRefreshLayout.setRefresh(isRefresh: Boolean) {
        this.isRefreshing = isRefresh
    }
}