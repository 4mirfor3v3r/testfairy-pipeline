package com.acemirr.consumerapp.viewmodel

import android.content.Context
import android.database.Cursor
import android.net.Uri
import android.util.Log
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.acemirr.consumerapp.model.Model

class MainActivityViewModel : ViewModel(){
    val list = ObservableField<ArrayList<Model>>(arrayListOf())
    var isLoading: ObservableField<Boolean> = ObservableField()

    private val tableName = "data"
    private val authority = "com.acemirr.learningpath4sub2"
    private val schema = "content"

    private val uri: Uri = Uri.Builder().scheme(schema)
        .authority(authority)
        .appendPath(tableName)
        .build()

    fun getData(context: Context){
        isLoading.set(true)
        Log.e("FETCHED","TRUE")
        val dataCursor = context.contentResolver.query(uri, null, null, null, null)
        if (dataCursor != null){
            mapCursorToArrayList(dataCursor){
                list.set(it)
                isLoading.set(false)
            }
            dataCursor.close()
        }
    }

    private fun mapCursorToArrayList(cursor: Cursor, onResult:(ArrayList<Model>) ->Unit) {
        val notesList: ArrayList<Model> = ArrayList()
        if (cursor.moveToFirst()){
            do {
                val id = cursor.getInt(cursor.getColumnIndexOrThrow("id"))
                val avatarUrl = cursor.getString(cursor.getColumnIndexOrThrow("avatar_url"))
                val username = cursor.getString(cursor.getColumnIndexOrThrow("username"))
                val name = cursor.getString(cursor.getColumnIndexOrThrow("name"))
                val location = cursor.getString(cursor.getColumnIndexOrThrow("location"))
                notesList.add(Model(id, avatarUrl, username, name, location))
                onResult(notesList)
            }while (cursor.moveToNext())
        }
        cursor.close()
    }
}