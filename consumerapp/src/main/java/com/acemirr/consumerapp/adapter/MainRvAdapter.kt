package com.acemirr.consumerapp.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.acemirr.consumerapp.R
import com.acemirr.consumerapp.databinding.MainListBinding
import com.acemirr.consumerapp.model.Model

class MainRvAdapter : ListAdapter<Model, MainRvAdapter.Holder>(diffUtilModelCallback) {
    class Holder(private val binding: MainListBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bindItem(user: Model) {
            binding.data = user

            binding.executePendingBindings()
        }
    }

    companion object {
        val diffUtilModelCallback = object : DiffUtil.ItemCallback<Model>() {
            override fun areItemsTheSame(oldItem: Model, newItem: Model): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(oldItem: Model, newItem: Model): Boolean {
                return oldItem.id == newItem.id
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val binding = DataBindingUtil.inflate<MainListBinding>(
            LayoutInflater.from(parent.context),
            R.layout.main_list,
            parent,
            false
        )

        return Holder(binding)
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        val user = getItem(position)
        holder.bindItem(user)
    }
}