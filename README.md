# Dicoding-Android-Learning-Path


## Screenshots

<p align="center">
  <img width="250" src="https://raw.githubusercontent.com/4mirfor3v3r/Dicoding-Android-Learning-Path/path-4-sub-3/arts/main.jpg" />
  <img width="250" src="https://raw.githubusercontent.com/4mirfor3v3r/Dicoding-Android-Learning-Path/path-4-sub-3/arts/detail-liked.jpg" /> 
  <img width="250" src="https://raw.githubusercontent.com/4mirfor3v3r/Dicoding-Android-Learning-Path/path-4-sub-3/arts/reminder.jpg" />
</p>
<p align="center">
  <img width="250" src="https://raw.githubusercontent.com/4mirfor3v3r/Dicoding-Android-Learning-Path/path-4-sub-3/arts/favorit.jpg" />
  <img width="250" src="https://raw.githubusercontent.com/4mirfor3v3r/Dicoding-Android-Learning-Path/path-4-sub-3/arts/search.jpg" />
  <img width="250" src="https://raw.githubusercontent.com/4mirfor3v3r/Dicoding-Android-Learning-Path/path-4-sub-3/arts/widget.jpg" />
</p>

## Builded With
 - ### Kotlin
 - ### Localization
 - ### MVVM(Model-View-ViewModel) ----- (Design Pattern)
 - ### Androidx Lifecycle [link](https://developer.android.com/jetpack/androidx/releases/lifecycle "link") ----- (Androidx Arch Component)
 - ### Paging [link](https://developer.android.com/jetpack/androidx/releases/paging "link") ----- (Androidx Arch Component)
 - ### Retrofit [link](https://square.github.io/retrofit/ "link") ----- (Networking)
 - ### Gson [link](https://github.com/google/gson "link") ----- (Json Converter)
 - ### KodeIn [link](https://kodein.org/Kodein-DI "link") -----  (Database Injection)
 - ### Room [link](https://developer.android.com/topic/libraries/architecture/room "link") ----- (local database)
 - ### Coroutines [link](https://github.com/Kotlin/kotlinx.coroutines "link") -----  (blocking + debounce + threading)
 - ### Recyclerview ListAdapter [link](https://developer.android.com/reference/androidx/recyclerview/widget/ListAdapter "link") -----  (Recyclerview ListAdapter Extensions)
 - ### Picasso [link](https://github.com/square/picasso "link") -----  (Image Caching)
 - ### SDP [link](https://github.com/intuit/sdp "link") -----  (Responsive Dimen Size)
 - ### SSP [link](https://github.com/intuit/ssp "link") -----  (Responsive Text Size)
 
 ## TODO
 - [x] Paging List
 - [x] Animated Recyclerview Scroll
 - [x] Auto Update widget with Job Scheduler
 - [x] Customable Reminder with AlarmManager
 - [ ] Clean Architecture
 - [ ] Automatic save to local database
 - [ ] Add Detail-Repos Section
 - [ ] Add SplashScreen
 - [ ] Icon App
